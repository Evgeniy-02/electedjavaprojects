import java.util.Scanner;

public class HashTable {

	// массив таблицы, каждый элемент указывает на цепочку ячеек
	private Cell[] table;
	// размер хеш-таблицы
	private int tableSize;

	// конструктор
	public HashTable(int size) {
		this.tableSize = size;
		table = new Cell[tableSize];
	}

	// конструктор по умолчанию
	public HashTable() {
		this(97);
	}

	// вычисление хеш-функции
	public int hash(int key) {
		return (key & 0x7fffffff) % tableSize;
	}

	// найти ячейку в таблице по значению ключа
	private Cell getCell(int key) {
		int slot = hash(key);
		Cell cell;
		for (cell = table[slot]; cell != null; cell = cell.next) {
			if (cell.key == key)
				return cell;
		}
		return cell;
	}

	// получить значение из таблицы по значению ключа
	// если значение не найдено, возвращается пустая строка
	public String get(int key) {
		Cell cell = getCell(key);
		if (cell == null)
			return "";

		return cell.value;
	}

	// добавление элемента в таблицу с ключом key
	// функция возвращает true в случае успеха
	public boolean put(int key, String value) {
		// игнорируем добавление элемента с существующим ключом
		if (getCell(key) != null)
			return false;

		// вычисляем хеш-функцию, добавляем элемент
		int slot = hash(key);
		if (table[slot] == null)
			table[slot] = new Cell(key, value);
		else
			table[slot] = new Cell(key, value, table[slot]);

		return true;
	}

	// удаление элемента из таблицы с ключом key
	public void remove(int key) {
		int slot = hash(key);
		Cell first = table[slot];
		// элемента нет, удалять нечего
		if (first == null)
			return;

		// специальный случай для первого элемента
		if (first.key == key) {
			table[slot] = first.next;
			return;
		}
		// удаление произвольного элемента
		for (Cell l = first.next; l != null; first = first.next, l = l.next) {
			if (l.key == key) {
				first.next = l.next;
				return;
			}
		}
	}

	// печать таблицы
	public void print() {
		System.out.println("\n---TABLE---\n");
		for (int index = 0; index < tableSize; ++index) {
			System.out.print("(" + index + ")");
			for (Cell cell = table[index]; cell != null; cell = cell.next) {
				System.out.print("-->[" + cell.key + ", " + cell.value + "]");
			}
			System.out.println("\n---");
		}
		System.out.println();
	}

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);

		int menuVal = 0;

		int size = 0;

		// запрашиваем размер хэш-таблицы
		while (size == 0) {
			System.out.println(
					"Введите размер таблицы (рекомендуется использовать простое число для уменьшения числа коллизий): ");
			size = s.nextInt();
		}

		// создаем таблицу
		HashTable hashTable = new HashTable(size);
		System.out.println("Создана хеш-таблица с размером: " + size);

		// организуем простое экранное меню
		do {
			try {
				System.out.println("\n>>>Меню<<<");
				System.out.println("0] Выход");
				System.out.println("1] Добавить один элемент в таблицу (строку)");
				System.out.println("2] Удалить один элемент из таблицы (строку)");
				System.out.println("3] Вывести хеш-таблицу на экран");
				System.out.println("4] Найти элемент в таблице");
				System.out.println("   Введите номер команды: ");

				menuVal = s.nextInt();

				String data = "";
				int key = 0;

				// выполняем действия в зависимости от выбранного пункта меню
				switch (menuVal) {
				case 0: // выход из программы
					System.out.println("... Работа завершена ...");
					break;
				case 1: // добавим элемент
					System.out.println(">>>Введите ключ для добавления элемента (число): ");
					key = s.nextInt();
					s.nextLine();
					System.out.println(">>>Введите элемент для добавления: ");
					data = s.nextLine();
					if (!hashTable.put(key, data)) { // добавляем элемент, если добавить невозможно, выводим сообщение
														// об ошибке
						System.err.println("Элемент не добавлен, т.к. он уже присутствует в таблице!\n");
						System.err.flush();
					} else {
						System.out.println("Готово!");
					}
					break;
				case 2: // удалим элемент
					System.out.println(">>>Введите ключ для удаления элемента (число): ");
					key = s.nextInt();
					hashTable.remove(key);
					break;
				case 3: // напечатаем таблицу
					hashTable.print();
					break;
				case 4: // найдем элемент в таблице по ключу
					System.out.println(">>>Введите ключ для поиска элемента (число): ");
					key = s.nextInt();
					data = hashTable.get(key);
					if (data.isEmpty()) { // элемент не найден
						System.err.println("Элемент не найден в таблице!\n");
						System.err.flush();
					} else { // элемент найден
						System.out.println("Элемент найден: " + data);
					}
					break;
				default: // пустое условие для остальных действий
					System.err.println("!!! Неверный номер команды !!!\n");
					System.err.flush();
					break;
				}
			} catch (Exception e) { // обрабатываются ошибки неправильного ввода
				System.err.println("!!! Ошибка ввода !!!\n");
				System.err.flush();
				s.nextLine();
			}
		} while (menuVal != 0); // условие выхода

	}

	// структура для внутреннего хранения строковых данных
	private class Cell {
		final String value;
		int key;
		Cell next;

		// конструктор
		// key - ключ, value - значение, next - ссылка на следующую ячейку
		public Cell(int key, String value, Cell next) {
			this.key = key;
			this.value = value;
			this.next = next;
		}

		public Cell(int key, String value) {
			this(key, value, null);
		}
	}

}
