import static java.lang.Math.*;

public class Oval extends Function {
	int i;

	public double valueAt(double x) {
		double y = 0;
		double r = 4;
		if (x > -r && x < r)
			y = i * sqrt(abs(x * x - r * r));
		else if (-r > x || x > r)
			y = Double.NaN;
		return y;
	}

	Oval(int d) {
		i = d;
	}

	public String toString() {
		String s = "  f(x) = " + "Oval(" + i + ")";
		return s;
	}

}
