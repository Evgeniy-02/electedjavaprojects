public abstract class Function {

	abstract double valueAt(double x);

	double doBisection(double a, double b, double e) {

		// System.out.println("a = " + a + ", b = " + b + ", e = " + e);

		double x = a + b / 2;
		// if (a > b) {
		// x = a;
		// a = b;
		// b = x;
		// }
		double v = valueAt(a) * valueAt(b);

		if (a >= b || v > 0)
			return Double.NaN;
		else if (v != 0) {
			while (Math.abs(b - a) > e) {
				if (valueAt(a) * valueAt(x) < 0)
					b = x;
				else
					a = x;

				x = (a + b) / 2;
				if (x == a || x == b)
					break;
			}
		} else if (valueAt(a) == 0)
			x = a;
		else
			x = b;

		return x;
	}
}