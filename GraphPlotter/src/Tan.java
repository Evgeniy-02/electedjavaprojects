public class Tan extends Function {

	public double valueAt(double x) {
		double y = 0;
		x = Math.toRadians(x * 60);
		y = Math.toDegrees((Math.tan(x))) / 60;

		return y;
	}
	
	public String toString() {
		String s = "tg(x)";
		s = "  f(x) = " + s;
		return s;
	}

}