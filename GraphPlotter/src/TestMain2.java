
public class TestMain2 {

	public static void main(String[] args) {
		double[] c = { 0, 0, 1 };
		double x = 16;
		Polinom p1 = new Polinom(c);
		double y = p1.valueAt(x);
		System.out.println("f(x) =" + p1.toString() + "\n");
		System.out.println("f(" + x + ") = " + y);
		// for (double i = 0; i <= 10; i += 0.1)
		// System.out.println("\n f(" + i + ") = " + p1.valueAt(i));
	}

}
