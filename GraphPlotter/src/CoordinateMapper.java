import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

public class CoordinateMapper {

	private Rectangle2D source;
	private Rectangle2D destination;

	public CoordinateMapper(Rectangle2D s, Rectangle2D d) {
		source = s;
		destination = d;
	}

	Point2D mapPoint(double x, double y) {
		double dx = Math.round((x - source.getX()) / source.getWidth() * destination.getWidth() + destination.getX());
		double dy = Math.round((y - source.getY()) / source.getHeight() * destination.getHeight() + destination.getY());
		return new Point2D.Double(dx, dy);
	}
}