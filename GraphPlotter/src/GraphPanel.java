import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.UIManager;

public class GraphPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ArrayList<Function> formulas;
	private Color[] formulaColors = { Color.DARK_GRAY, Color.RED, Color.GREEN, Color.ORANGE, Color.MAGENTA, Color.GRAY,
			Color.PINK, Color.BLACK, Color.YELLOW };

	private double fromX = -10;
	private double toX = 10;
	private double fromY = -20;
	private double toY = 20;
	int w;
	int h;
	CoordinateMapper mapper;

	private Point dragStartPoint;
	private Point panelOffset = new Point(0, 0);

	GraphPanel() {
		formulas = new ArrayList<Function>();

		/*
		addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
				dragStartPoint = e.getPoint();
				dragStartPoint.translate(-panelOffset.x, -panelOffset.y);
			}

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

		});

		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				Point p = e.getPoint();
				p.translate(-dragStartPoint.x, -dragStartPoint.y);
				offsetPanelBy(p);
			}
		});*/
	}

	// центрировать сетку
	public void center() {
		Point p = new Point(0, 0);
		offsetPanelBy(p);
	}

	protected void offsetPanelBy(Point offset) {
		panelOffset = offset;
		repaint();
	}

	void addFormula(Function p) {
		for (Function t : formulas) {
			if (t.equals(p))
				return;
		}
		formulas.add(p);
		repaint();
	}

	ArrayList<Function> getFormulas() {
		return formulas;
	}

	void addScale(double t) {
		toX = t;
		fromX = -toX;
		repaint();
	}

	public void paintComponent(Graphics g) {
		// super.paintComponent(g);
		w = getWidth();
		h = getHeight();
		fromY = -(double) h / w * (toX - fromX) / 2;
		toY = (double) h / w * (toX - fromX) / 2;

		mapper = new CoordinateMapper(new Rectangle2D.Double(fromX, fromY, toX - fromX, toY - fromY),
				new Rectangle2D.Double(panelOffset.x, panelOffset.y + h, w, -h));

		g.setColor(Color.WHITE);
		g.fillRect(0, 0, w, h);

		// g.drawOval(450, 300, 400, 150);
		// g.drawRect(100, 100, 300, 300);
		paintAxes(g);

		g.setColor(Color.BLUE);
		g.drawRect(0, 0, w - 1, h - 1);

		for (int i = 0; i < formulas.size(); i++) {
			Function f = formulas.get(i);
			paintFormula(f, g, formulaColors[i % formulaColors.length]);

			if (formulas.get(i) instanceof Polinom) {
				try {
					Polinom p = (Polinom) formulas.get(i);
					double[] roots = p.solve(fromX, toX);
					for (int k = 0; k < roots.length; k++) {
						paintRoots(roots[k], g);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void paintFormula(Function f, Graphics g, Color formulaColor) {
		g.setColor(formulaColor);
		double step = (toX - fromX) / 2000;

		for (double x = fromX; x < toX - step; x += step) {
			if (!Double.isNaN(f.valueAt(x)) && !Double.isNaN(f.valueAt(x + step))) {
				boolean nan1 = Double.isNaN(f.valueAt(x));
				boolean nan2 = Double.isNaN(f.valueAt(x + step));
				if (!nan1 && !nan2) {
					Point2D p1 = mapper.mapPoint(x, f.valueAt(x));
					Point2D p2 = mapper.mapPoint(x + step, f.valueAt(x + step));
					int x1 = (int) p1.getX();
					int y1 = (int) p1.getY();
					int x2 = (int) p2.getX();
					int y2 = (int) p2.getY();
					g.drawLine(x1, y1, x2, y2);
				} else if (nan1 && nan2)// два последовательных NaN
				{
					break;
				}
			}
		}
	}

	private void paintAxes(Graphics g) {
		/**
		 * Координатная сетка и координатные оси
		 */
		g.setColor(UIManager.getColor("Label.background"));
		Rectangle2D r = new Rectangle2D.Double(0, 0, getWidth(), getHeight());

		/* здесь отрисовка осей X */
		for (int i = (int) fromY; i <= (int) toY; i += 1) {
			Point2D p = mapper.mapPoint(fromX, i);
			Point2D p1 = mapper.mapPoint(toX, 0);

			if (i == 0) {
				g.setColor(Color.BLUE);
				g.fillPolygon(new int[] { w - 12, w, w - 12 },
						new int[] { (int) p.getY() - 4, (int) p.getY(), (int) p.getY() + 4 }, 3);// стрелка на оси X
			} else
				g.setColor(UIManager.getColor("Label.background"));

			g.drawLine((int) p.getX(), (int) p.getY(), (int) p1.getX(), (int) p.getY());
		}
		/* здесь отрисовка осей Y */
		for (int i = (int) fromX; i <= (int) toX; i += 1) {
			Point2D p = mapper.mapPoint(i, fromY);

			if (i == 0) {
				g.setColor(Color.BLUE);
				g.fillPolygon(new int[] { (int) p.getX() - 4, (int) p.getX(), (int) p.getX() + 4 },
						new int[] { 12, 0, 12 }, 3);// стрелка на оси Y
			} else
				g.setColor(UIManager.getColor("Label.background"));

			g.drawLine((int) p.getX(), 0, (int) p.getX(), (int) p.getY());
		}

		// Код ниже нужно переместить в функции paintXAxis и paintYAxis.
		// Вызывать их из циклов выше передавая туда правильные (пропущенные через
		// mapper) координаты их расположения.

		// /**
		// * Координатные оси
		// */
		g.setColor(Color.BLUE);
		// g.drawLine(0, h / 2, w, h / 2);
		// g.drawLine(w / 2, 0, w / 2, h);

		// /**
		// * Стрелки
		// */
		// g.fillPolygon(new int[] { w - 12, w, w - 12 }, new int[] { h / 2 - 4, h / 2,
		// h / 2 + 4 }, 3);
		// g.fillPolygon(new int[] { w / 2 - 4, w / 2, w / 2 + 4 }, new int[] { 12, 0,
		// 12 }, 3);

		/**
		 * Отрисовка тик-марок
		 */
		for (int i = (int) fromX; i <= (int) toX; i += 1) {
			if (i != 0) {
				Point2D p = mapper.mapPoint(i, 0);
				g.drawLine((int) p.getX(), (int) p.getY() + 3, (int) p.getX(), (int) p.getY() - 3);
			}
		}
		for (int i = (int) fromY; i <= (int) toY; i += 1) {
			if (i != 0) {
				Point2D p = mapper.mapPoint(0, i);
				g.drawLine((int) p.getX() - 3, (int) p.getY(), (int) p.getX() + 3, (int) p.getY());
			}
		}

	}

	/**
	 * Отрисовка корней
	 */
	private void paintRoots(double r, Graphics g) {
		g.setColor(Color.RED);
		Point2D p1 = mapper.mapPoint(r, 0);
		final int h = 4;
		g.fillOval((int) p1.getX() - h, (int) p1.getY() - h, h * 2, h * 2);
	}

	public void clear() {
		formulas.clear();
		repaint();
	}

	public void clear1() {
		if (formulas.size() != 0)
			formulas.remove(formulas.size() - 1);
		repaint();
	}

	//
	public void clear(int i) {
		if (i >= 0 && i < formulas.size()) {
			formulas.remove(i);
		}
		repaint();
	}
}
