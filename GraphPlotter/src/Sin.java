public class Sin extends Function {

	public double valueAt(double x) {
		double y = 0;
		x = Math.toRadians(x * 60);
		y = Math.toDegrees((Math.sin(x))) / 60;

		return y;
	}

	public String toString() {
		String s = "sin(x)";
		s = "  f(x) = " + s;
		return s;
	}

}