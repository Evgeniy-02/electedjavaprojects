import static java.lang.Math.pow;

public class Cos extends Function {

	public String toString() {
		String s = "cos(x)";
		s = "  f(x) = " + s;
		return s;
	}
	
	/*
	 //через ряд Тейлора
	 public double valueAt(double x) {
		double y = 1;
		byte z = 1;
		for (int r = 1; r <= 30; r++) {
			z *= -1;
			double s = z * pow(x, 2 * r) / fac(2 * r);
			y += s;
		}
		return y;

	}*/
	
	public double valueAt(double x) {
		double y = 0;
		x = Math.toRadians(x * 60);
		y = Math.toDegrees((Math.sin(x))) / 60;

		return y;
	}

	double fac(int f) {
		double x = 1;
		for (int i = 2; f >= i; i++)
			x *= i;
		return x;
	}


}