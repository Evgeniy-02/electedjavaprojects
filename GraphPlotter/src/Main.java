import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.AbstractAction;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * Программа для построения графиков и нахождения корней функции.
 * 
 * @version 0.4.9 25-05-2017
 * @author Evgeniy Lysenko
 */

public class Main {

	GraphPanel graphPanel;
	private JFrame frmCoordinatePlotter;
	private JTextField textFieldA;
	private JTextField textFieldB;
	private JTextField textFieldC;
	private JTextField textFieldD;
	private JTextField textFieldE;
	private JTextField textFieldF;
	private JTextField textFieldG;
	private JTextField textFieldH;
	String a;
	private JSlider slider;
	private JPanel panel;
	private JComboBox comboBox;
	private JButton addButton;
	private Function[] defaultFunctions;
	int toX;
	private JButton ButtonCenter;
	private JButton btnNewButton_2;
	private JButton btnNewButton_3;
	private JButton btnNewButton_4;
	private JButton btnNewButton_5;
	private JTextField textField;
	private JMenuBar menuBar;
	JMenu functionsMenu;
	private JMenuItem listItem;
	JList functionsList;
	private DefaultListModel listModel;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					// LookAndFeelInfo[] list =
					// UIManager.getInstalledLookAndFeels();
					// for (Object k : list) {
					// System.out.println(k);
					// }
					UIManager.setLookAndFeel(UIManager.getInstalledLookAndFeels()[1].getClassName());

					// UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					// JFrame.setDefaultLookAndFeelDecorated(true);

					Main window = new Main();
					window.frmCoordinatePlotter.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Main() {
		initialize();
	}

	static double tryParseDouble(String s) {
		try {
			return Double.parseDouble(s);
		} catch (Exception e) {
			return 0;
		}
	}

	Polinom addPolinom() {
		double a = tryParseDouble(textFieldA.getText());
		double b = tryParseDouble(textFieldB.getText());
		double c = tryParseDouble(textFieldC.getText());
		double d = tryParseDouble(textFieldD.getText());
		double e = tryParseDouble(textFieldE.getText());
		double f = tryParseDouble(textFieldF.getText());
		double g = tryParseDouble(textFieldG.getText());
		double h = tryParseDouble(textFieldH.getText());

		double[] m = { h, g, f, e, d, c, b, a };
		Polinom p = new Polinom(m);
		if (a != 0 || b != 0 || c != 0 || d != 0 || e != 0 || f != 0 || g != 0 || h != 0) {
			graphPanel.addFormula(p);
			updateMenu();
			return p;
		}
		return null;
	}

	private void updateMenu() {
		functionsMenu.removeAll();

		ArrayList<Function> formulas = graphPanel.getFormulas();
		for (int i = 0; i < formulas.size(); i++) {
			final int index = i;
			functionsMenu.add(new JMenuItem(new AbstractAction(formulas.get(i).toString()) {
				public void actionPerformed(ActionEvent ae) {
					graphPanel.clear(index);
					updateMenu();
				}
			}));
		}
	}

	private void rememberPolinom(Polinom p) {
		// добавить р в defaultFunctions
		defaultFunctions = Arrays.copyOf(defaultFunctions, defaultFunctions.length + 1);
		defaultFunctions[defaultFunctions.length - 1] = p;

		// обновить comboBox
		comboBox.setModel(new DefaultComboBoxModel(defaultFunctions));
		comboBox.setSelectedItem(p);
	}

	void clearText() {
		textFieldA.setText(null);
		textFieldB.setText(null);
		textFieldC.setText(null);
		textFieldD.setText(null);
		textFieldE.setText(null);
		textFieldF.setText(null);
		textFieldG.setText(null);
		textFieldH.setText(null);
	}

	private void initialize() {

		defaultFunctions = new Function[] { new Sin(), new Cos(), new Oval(1), new Oval(-1), new Polinom(0, 1),
				new Polinom(0, 0, 1), new Polinom(0, 0, 0, 1), new Polinom(0, 0, 0, 0, 1),
				new Polinom(0, 0, 0, 0, 0, 1), new Polinom(0, 0, 1, 0, 0, 1), new Polinom(1, 0, -4, 0, 1),
				new Polinom(4, 0, -5, 0, 1), new Polinom(0.0442, -0.213, -2.27, -0.3, 1),
				new Polinom(-3.23323, -0.5631, 6.550, -1.30, -2.7, 1) };

		// Function defaultFunctions=(Function) graphPanel.formulas;

		frmCoordinatePlotter = new JFrame();
		frmCoordinatePlotter.setTitle("Построитель графиков");
		frmCoordinatePlotter.setBounds(90, 90, 1100, 600);
		frmCoordinatePlotter.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		menuBar = new JMenuBar();
		frmCoordinatePlotter.getContentPane().add(menuBar, BorderLayout.NORTH);

		JMenu PlotterMenu = new JMenu("График");
		PlotterMenu.setIcon(new ImageIcon(
				Main.class.getResource("/com/sun/javafx/scene/control/skin/modena/HTMLEditor-Justify-Black.png")));
		menuBar.add(PlotterMenu);

		JMenuItem clearItem = new JMenuItem("Очистить поля");
		clearItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clearText();
			}
		});
		PlotterMenu.add(clearItem);

		functionsMenu = new JMenu("Удалить");
		PlotterMenu.add(functionsMenu);

		JMenuItem clear1Item = new JMenuItem("Убрать графики");
		clear1Item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				graphPanel.clear1();
			}
		});
		PlotterMenu.add(clear1Item);

		listItem = new JMenuItem("Список");
		listItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listModel = new DefaultListModel();

				if (graphPanel.getFormulas().isEmpty())
					listModel.addElement("<Пусто>");
				for (Function f : graphPanel.getFormulas()) {
					listModel.addElement(f);
				}
				functionsList = new JList<String>(listModel);
				functionsList.setSelectedIndex(0);
				JScrollPane sp = new JScrollPane(functionsList);
				sp.setPreferredSize(new Dimension(400, 250));

				// String[] options = new String[] { "Очистить", "Close" };
				// JOptionPane.showOptionDialog(null, sp, "Список",
				// JOptionPane.NO_OPTION, JOptionPane.PLAIN_MESSAGE, null,
				// options, options[1]);

				JFrame listFrame = new JFrame();
				listFrame.setModalExclusionType(Dialog.ModalExclusionType.NO_EXCLUDE);
				listFrame.setTitle("Список функций");
				listFrame.setBounds(90, 90, 500, 350);
				listFrame.getContentPane().add(sp, BorderLayout.CENTER);
				// listFrame.set
				listFrame.setVisible(true);
			}
		});
		PlotterMenu.add(listItem);

		JMenuItem prefItem = new JMenuItem("Настройки");
		PlotterMenu.add(prefItem);

		PlotterMenu.addSeparator();

		JMenuItem exitItem = new JMenuItem("Завершить");
		exitItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		PlotterMenu.add(exitItem);

		JPanel mainPanel = new JPanel();
		frmCoordinatePlotter.getContentPane().add(mainPanel, BorderLayout.CENTER);
		mainPanel.setLayout(new BorderLayout(0, 0));

		JPanel LblPanel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) LblPanel.getLayout();
		// LblPanel.setBackground(new Color(255, 215, 0));

		JLabel lblA = new JLabel("f(x) =");
		lblA.setFont(new Font("Dialog", Font.BOLD, 14));
		LblPanel.add(lblA);

		textFieldA = new JTextField();
		textFieldA.setHorizontalAlignment(SwingConstants.TRAILING);
		// textFieldA.setBackground(Color.WHITE);
		textFieldA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addPolinom();
			}
		});
		LblPanel.add(textFieldA);
		textFieldA.setColumns(5);

		JLabel lblB = new JLabel("· x⁷+");
		lblB.setFont(new Font("Dialog", Font.BOLD, 14));
		LblPanel.add(lblB);

		textFieldB = new JTextField();
		textFieldB.setHorizontalAlignment(SwingConstants.TRAILING);
		// textFieldB.setBackground(Color.WHITE);
		textFieldB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addPolinom();
			}
		});
		LblPanel.add(textFieldB);
		textFieldB.setColumns(5);

		JLabel lblC = new JLabel("· x⁶+");
		lblC.setFont(new Font("Dialog", Font.BOLD, 14));
		LblPanel.add(lblC);

		textFieldC = new JTextField();
		textFieldC.setHorizontalAlignment(SwingConstants.TRAILING);
		// textFieldC.setBackground(Color.WHITE);
		textFieldC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addPolinom();
			}
		});
		LblPanel.add(textFieldC);
		textFieldC.setColumns(5);

		JLabel lblD = new JLabel("· x⁵+");
		lblD.setFont(new Font("Dialog", Font.BOLD, 14));
		LblPanel.add(lblD);

		textFieldD = new JTextField();
		textFieldD.setHorizontalAlignment(SwingConstants.TRAILING);
		// textFieldD.setBackground(Color.WHITE);
		textFieldD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addPolinom();
			}
		});
		LblPanel.add(textFieldD);
		textFieldD.setColumns(5);

		JLabel lblE = new JLabel("· x⁴+");
		lblE.setFont(new Font("Dialog", Font.BOLD, 14));
		LblPanel.add(lblE);

		textFieldE = new JTextField();
		textFieldE.setHorizontalAlignment(SwingConstants.TRAILING);
		// textFieldE.setBackground(Color.WHITE);
		textFieldE.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addPolinom();
			}
		});
		LblPanel.add(textFieldE);
		textFieldE.setColumns(5);

		JLabel lblF = new JLabel("· x³+");
		lblF.setFont(new Font("Dialog", Font.BOLD, 14));
		LblPanel.add(lblF);

		textFieldF = new JTextField();
		textFieldF.setHorizontalAlignment(SwingConstants.TRAILING);
		// textFieldF.setBackground(Color.WHITE);
		textFieldF.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addPolinom();
			}
		});
		LblPanel.add(textFieldF);
		textFieldF.setColumns(5);

		JLabel lblG = new JLabel("· x²+");
		lblG.setFont(new Font("Dialog", Font.BOLD, 14));
		LblPanel.add(lblG);

		textFieldG = new JTextField();
		textFieldG.setHorizontalAlignment(SwingConstants.TRAILING);
		// textFieldG.setBackground(Color.WHITE);
		textFieldG.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addPolinom();
			}
		});
		LblPanel.add(textFieldG);
		textFieldG.setColumns(5);

		JLabel lblH = new JLabel("· x+");
		lblH.setFont(new Font("Dialog", Font.BOLD, 14));
		LblPanel.add(lblH);

		textFieldH = new JTextField();
		textFieldH.setHorizontalAlignment(SwingConstants.TRAILING);
		// textFieldH.setBackground(Color.WHITE);
		textFieldH.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addPolinom();
			}
		});
		LblPanel.add(textFieldH);
		textFieldH.setColumns(5);

		addButton = new JButton("Добавить");
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Polinom r = addPolinom();
				if (r != null)
					if (r.order() > 0) {
						rememberPolinom(r);
					}
				clearText();
			}
		});
		LblPanel.add(addButton);

		JScrollPane scrollPanel = new JScrollPane(LblPanel);
		scrollPanel.setPreferredSize(new Dimension(100, 60));
		mainPanel.add(scrollPanel, BorderLayout.NORTH);

		panel = new JPanel();
		mainPanel.add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));

		JPanel functionPanel = new JPanel();
		// functionPanel.setBorder(new MatteBorder(1, 1, 0, 1, (Color) new
		// Color(0, 0, 0)));
		functionPanel.setLayout(new BorderLayout(0, 0));
		panel.add(functionPanel, BorderLayout.NORTH);

		comboBox = new JComboBox();
		comboBox.setFont(new Font("Dialog", Font.BOLD, 13));
		// comboBox.setEditable(true);
		// comboBox.setFont(new Font("Dialog", Font.BOLD, 16));
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Object o = comboBox.getSelectedItem();
				if (o instanceof Function) {
					Function f = (Function) o;
					graphPanel.addFormula(f);
					updateMenu();
				}
			}
		});
		comboBox.setModel(new DefaultComboBoxModel(defaultFunctions));
		functionPanel.add(comboBox);

		JButton btnDelete = new JButton("  Очистить график  ");
		// btnDelete.setIcon(new
		// ImageIcon(MainFrame.class.getResource("/javax/swing/plaf/metal/icons/ocean/warning.png")));
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				graphPanel.clear();
			}
		});
		functionPanel.add(btnDelete, BorderLayout.EAST);

		graphPanel = new GraphPanel();
		graphPanel.addMouseWheelListener(new MouseWheelListener() {
			public void mouseWheelMoved(MouseWheelEvent e) {
				if (e.getWheelRotation() < 0) {
					toX = slider.getValue();
					toX += 1;
					graphPanel.addScale(toX);
				} else if (e.getWheelRotation() > 0) {
					toX = slider.getValue();
					toX -= 1;
				}
				slider.setValue(toX);
			}
		});
		// graphPanel.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		panel.add(graphPanel, BorderLayout.CENTER);

		slider = new JSlider();
		mainPanel.add(slider, BorderLayout.WEST);
		slider.setOrientation(SwingConstants.VERTICAL);
		// slider.setBorder(new LineBorder(Color.WHITE, 1, true));
		slider.setValue(8);
		graphPanel.addScale(slider.getValue());
		slider.setMinimum(2);
		slider.setMaximum(50);
		slider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				toX = slider.getValue();
				graphPanel.addScale(toX);
			}
		});

		JPanel toolsPanel = new JPanel();
		mainPanel.add(toolsPanel, BorderLayout.EAST);
		// toolsPanel.setBorder(new MatteBorder(1, 0, 1, 1, (Color) new Color(0,
		// 0, 0)));
		// toolsPanel.setBackground(Color.WHITE);
		toolsPanel.setLayout(new BoxLayout(toolsPanel, BoxLayout.PAGE_AXIS));
		//TODO
		toolsPanel.setVisible(false);
 
		ButtonCenter = new JButton("Центрировать");
		ButtonCenter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				graphPanel.center();
			}
		});
		ButtonCenter.setAlignmentX(Component.CENTER_ALIGNMENT);
		toolsPanel.add(ButtonCenter);

		btnNewButton_2 = new JButton("New button");
		btnNewButton_2.setAlignmentX(Component.CENTER_ALIGNMENT);
		toolsPanel.add(btnNewButton_2);

		btnNewButton_3 = new JButton("New button");
		btnNewButton_3.setAlignmentX(Component.CENTER_ALIGNMENT);
		toolsPanel.add(btnNewButton_3);

		btnNewButton_4 = new JButton("New button");
		btnNewButton_4.setAlignmentX(Component.CENTER_ALIGNMENT);
		toolsPanel.add(btnNewButton_4);

		btnNewButton_5 = new JButton("New button");
		btnNewButton_5.setAlignmentX(Component.CENTER_ALIGNMENT);
		toolsPanel.add(btnNewButton_5);

		JPanel SEpanel = new JPanel();
		mainPanel.add(SEpanel, BorderLayout.SOUTH);
		// panel_1.setBackground(new Color(218, 165, 32));

		textField = new JTextField();
		textField.setText("Math.abs(Math.sin(x))+Math.tan(x)");
		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String d = textField.getText();
				try {
					graphPanel.addFormula(new ScriptEvaluator(d));
				} catch (Error error) {
					error.printStackTrace();
				}
			}
		});
		SEpanel.add(textField);
		textField.setColumns(50);

		/**
		 * toolsBar = new JToolBar(); toolsBar.setOrientation(SwingConstants.VERTICAL);
		 * frmCoordinatePlotter.getContentPane().add(toolsBar, BorderLayout.EAST);
		 */
		// FunctionPlotter();
	}

	void FunctionPlotter() {
		double[] c = { 0, 0, 1 };
		graphPanel.addFormula(new Polinom(c));

		double[] s = { 0, 0.2, 8, 12 };
		graphPanel.addFormula(new Polinom(s));

		double[] m = { 0, 1 };
		graphPanel.addFormula(new Polinom(m));

		graphPanel.addFormula(new Oval(1));
		graphPanel.addFormula(new Oval(-1));

		graphPanel.addFormula(new Sin());
		graphPanel.addFormula(new Tan());

		String d = "x-2";
		graphPanel.addFormula(new ScriptEvaluator(d));
	}
}
