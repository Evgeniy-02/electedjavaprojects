import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public class ScriptEvaluator extends Function {

	private String script;
	private ScriptEngine engine;

	public ScriptEvaluator(String script) {
		super();
		this.script = script;

		ScriptEngineManager factory = new ScriptEngineManager();
		engine = factory.getEngineByName("JavaScript");
	}

	public double valueAt(double x) {
		engine.put("x", x);
		Object o = null;
		try {
			o = engine.eval(script);
			Integer result = (Integer) o;
			return result.doubleValue();
		} catch (Exception e1) {
			try {
				Double result = (Double) o;
				return result.doubleValue();
			} catch (Exception e2) {
				e2.printStackTrace();
				return Double.NaN;
			}
		}
	}

	public String toString() {

		return "  f(x) = " + script;
	}

}
