
public class TestMain {
	static void testDoBisection(double[] c) {

		Polinom p = new Polinom(c);

		double r = p.doBisection(0, 3, 0);

		System.out.println(r);
		System.out.println(p.valueAt(r));
	}

	private static void testDerivate(double... c) {
		Polinom p = new Polinom(c);
		Polinom pd = p.derivate();

		System.out.println("функция = " + p);
		System.out.println("производная" + pd);
	}

	public static void main(String[] args) {
		double[] c = { -2, 0, 1 };

		testDoBisection(c);

		// testDerivate(1, -2, 1, 2);
		// testDerivate(-1, 0, 1);
	}

}
