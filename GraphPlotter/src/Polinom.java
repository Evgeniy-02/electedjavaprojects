import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Polinom extends Function {
	double[] a;

	public double valueAt(double x) {
		double res = 0;
		double p = 1;
		for (int i = 0; i < a.length; i++) {
			res += a[i] * p;
			p *= x;
		}
		return res;
	}

	Polinom(double... args) {
		a = args;
	}

	int order() {
		int i = a.length - 1;
		do {
			if (a[i] != 0)
				return i;
		} while (--i >= 0);
		return 0;
	}

	public boolean equals(Object p) {
		return super.equals(p) || p instanceof Polinom && Arrays.equals(a, ((Polinom) p).a);
	}

	public String toString() {
		String s = "";
		for (int i = a.length - 1; i >= 0; i--) {
			if (a[i] != 0) {
				if (s.length() > 0 && a[i] >= 0)
					s += "+";
				if (a[i] != 1) {
					s += a[i] + (i > 0 ? "*" : "");
				} else if (i == 0) {
					s += a[i];
				}
				if (i > 1) {
					s += "x\u02c4" + i;
				} else if (i > 0) {
					s += "x";
				}
			}
		}
		s = "  f(x) = " + s + "\n";
		return s;
	}

	Polinom derivate() /* производная */ {
		double[] s = new double[a.length - 1];
		for (int i = 0; i < s.length; i++)
			s[i] = (i + 1) * a[i + 1];

		return new Polinom(s);
	}

	double[] solve(double a, double b) {
		ArrayList<Double> result = new ArrayList<Double>();

		if (order() == 0) {
			return new double[0];
		} else if (order() == 1) {
			return new double[] { -this.a[0] / this.a[1] };
		}

		Polinom pd = derivate();
		System.out.println(pd);
		double[] rd = pd.solve(a, b);
		System.out.println(Arrays.toString(rd));

		double[] ranges = new double[rd.length + 2];
		ranges[0] = a;
		ranges[ranges.length - 1] = b;
		for (int i = 0; i < rd.length; i++) {
			ranges[i + 1] = rd[i];
		}

		for (int i = 0; i < ranges.length - 1; i++) {
			double val1 = valueAt(ranges[i]);
			double val2 = valueAt(ranges[i + 1]);

			if (val1 == 0) {
				result.add(ranges[i]);
			}

			if (val2 == 0) {
				result.add(ranges[i + 1]);
			}

			if (val1 * val2 < 0) {
				double root = doBisection(ranges[i], ranges[i + 1], 0);
				result.add(root);
				System.out.println("x = " + root);
			}
		}

		return toDouble(result);
	}

	double[] toDouble(ArrayList<Double> doubles) {
		double[] res = new double[doubles.size()];

		for (int i = 0; i < doubles.size(); i++) {
			res[i] = doubles.get(i);
		}

		return res;
	}

}
