package components;

import static java.lang.Math.PI;
import static java.lang.Math.abs;
import static java.lang.Math.toDegrees;

import application.Application;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.Timeline;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Scale;
import javafx.util.Duration;
import utils.CoordinateMapper;
import utils.Coordinates;

public class CirclesWheel extends Group {
	public final int COUNT_OF_CIRCLES;

	private ItemCircle[] circles;
	private double[] circlesAngles;

	private Rotate rotate;
	private Scale[] scaleTransforms;

	/**
	 * positive clockwise direction 1 - positive -1 - negative
	 */
	private IntegerProperty directionOfRotation = new SimpleIntegerProperty(1);

	private double previousAngle;
	private double angle;

	public CirclesWheel(int countOfCircles) {
		COUNT_OF_CIRCLES = countOfCircles;
		circles = new ItemCircle[COUNT_OF_CIRCLES];
		for (int i = 0; i < circles.length; i++) {

			circles[i] = ItemCircle.createLoketCircle(
					new Image(Application.class.getResourceAsStream(i == 0 ? "ball.jpg" : "smile.jpg")));

			ItemCircle c = circles[i];

			c.setOnMouseReleased(e -> {
				RotateTransition rt = new RotateTransition(Duration.millis(700), c);
				rt.setByAngle((Math.random() - 0.5 > 0 ? 1 : -1) * 360);
				rt.setCycleCount(2);
				rt.setAutoReverse(true);

				rt.play();
			});

			c.getStyleClass().add("circle");

			this.getChildren().add(c);
		}

		rotate = new Rotate(0);
		InvalidationListener listener = new InvalidationListener() {

			private double deltaAngle = 360 / countOfCircles;

			@Override
			public void invalidated(Observable observable) {
				double wheelAngle = rotate.getAngle();

				System.out.print("[");
				for (int i = 0; i < circles.length; i++) {
					double newCircleAngle = (circlesAngles[i] + wheelAngle - 90) % 360;
					if (newCircleAngle > 180) {
						newCircleAngle -= 360;
					}

					double scaleValue = f(newCircleAngle);
//					System.out.printf("[%.2f, %.2f], ", newCircleAngle, scaleValue);
//					System.out.print(scaleTransforms[i].getX() + ", ");

					scaleTransforms[i].setX(scaleValue);
					scaleTransforms[i].setY(scaleValue);

				}
				System.out.println("]");
			}

			private double f(double a) {
				double res = abs(a) > deltaAngle / 2 ? 1 : 1.5 - abs(a) / (deltaAngle / 2) * (5.0 / 10);
				return res;
			}
		};
		rotate.angleProperty().addListener(listener);
		this.getTransforms().add(rotate);

		scaleTransforms = new Scale[circles.length];
		for (int i = 0; i < scaleTransforms.length; i++) {
			scaleTransforms[i] = new Scale();
			circles[i].getTransforms().add(scaleTransforms[i]);
		}

		circlesAngles = new double[COUNT_OF_CIRCLES];

	}

	public void updatePositionAndSize(double centerX, double centerY, double wheelRadius, double itemCircleRadius) {

		CoordinateMapper mapper = new CoordinateMapper();
		mapper.setGridCenter(new Point2D(centerX, centerY));

		double angleOffset = 2 * PI / COUNT_OF_CIRCLES;
		for (int i = 0; i < COUNT_OF_CIRCLES; i++) {
			double angrad = i * angleOffset + PI / 2;

			circles[i].setRadius(itemCircleRadius);
			Point2D center = mapper.mapToPaneCoordinates(Coordinates.toCartesianSystem(wheelRadius, -angrad));
			circles[i].setCenterXY(center);

			circlesAngles[i] = toDegrees(angrad);

			scaleTransforms[i].setPivotX(center.getX());
			scaleTransforms[i].setPivotY(center.getY());
		}
		scaleTransforms[0].setX(1.5);
		scaleTransforms[0].setY(1.5);

		rotate.setPivotX(centerX);
		rotate.setPivotY(centerY);
	}

	public DoubleProperty getRotateProperty() {
		DoubleProperty rotProperty = rotate.angleProperty();

		rotProperty.addListener((angle, oldvalue, newvalue) -> {
			for (int i = 0; i < circles.length; i++)
				circles[i].setRotate(-newvalue.doubleValue());
		});

		return rotProperty;
	}

	/**
	 * positive clockwise direction
	 * 
	 * @param deltaAngle in degrees
	 */
	public void turnWheel(double deltaAngle) {
		angle += deltaAngle;

		directionOfRotation.set(angle > previousAngle ? 1 : -1);
//		System.out.println("direction: " + directionOfRotation.get());

		getRotateProperty().set(angle);

		previousAngle = angle;

	}

	private void fixateRotation() {
		angle %= 360;
		getRotateProperty().set(angle);
	}

	public void startRotationAnimation(double speedRatio) {
		fixateRotation();

		Timeline timeline = new Timeline();

		double deltaAngle = 360 / COUNT_OF_CIRCLES;
		double brakingDistanceInDegrees = directionOfRotation.get() == 1 ? angle + deltaAngle - (angle % deltaAngle)
				: angle < 0 ? angle - deltaAngle - (angle % deltaAngle) : angle - (angle % deltaAngle);

		// System.out.println("angle from: " + getRotateProperty().get() + " to " +
		// brakingDistanceInDegrees);

		KeyValue keyValue = new KeyValue(getRotateProperty(), brakingDistanceInDegrees, Interpolator.EASE_OUT);

		EventHandler onFinished = t -> angle = getRotateProperty().get() % 360;

		KeyFrame keyFrame = new KeyFrame(Duration.millis(700), onFinished, keyValue);

		timeline.getKeyFrames().add(keyFrame);
		timeline.play();

	}

}
