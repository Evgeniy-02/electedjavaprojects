package components;

import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.RotateTransition;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Arc;
import javafx.scene.shape.Circle;
import javafx.util.Duration;

public class ItemCircle extends Group {

	Arc arc1 = new Arc();

	Arc arc2 = new Arc();
	Arc arc3 = new Arc();
	Arc arc41 = new Arc();
	Arc arc42 = new Arc();
	Group arc4gr = new Group();
	Arc arc51 = new Arc();
	Arc arc52 = new Arc();
	Group arc5gr = new Group();
	Circle front;

	public ItemCircle(Circle front) {
		this.front = front;

		createArcs();

		getChildren().addAll(front, arc1, arc2, arc3, arc4gr, arc5gr);

		getStyleClass().add("item");
	}

	private void createArcs() {
		arc1.setStartAngle(60);
		arc1.setLength(310);
		arc1.setStrokeWidth(2);
		arc1.setFill(null);
		arc1.getStyleClass().add("arc");

		arc2.setStartAngle(0);
		arc2.setLength(275);
		arc2.setStrokeWidth(6);
		arc2.setFill(null);
		arc2.getStyleClass().add("arc");

		arc3.setStartAngle(180);
		arc3.setLength(310);
		arc3.setStrokeWidth(2);
		arc3.setFill(null);
		arc3.getStyleClass().add("arc");

		arc41.setStartAngle(270);
		arc41.setLength(165);
		arc41.setStrokeWidth(6);
		arc41.setFill(null);
		arc41.getStyleClass().add("arc");
		arc42.setStartAngle(90);
		arc42.setLength(165);
		arc42.setStrokeWidth(6);
		arc42.setFill(null);
		arc42.getStyleClass().add("arc");
		arc4gr.getChildren().addAll(arc41, arc42);

		arc51.setStartAngle(250);
		arc51.setLength(165);
		arc51.setStrokeWidth(2);
		arc51.setFill(null);
		arc51.getStyleClass().add("arc");
		arc52.setStartAngle(70);
		arc52.setLength(165);
		arc52.setStrokeWidth(2);
		arc52.setFill(null);
		arc52.getStyleClass().add("arc");
		arc5gr.getChildren().addAll(arc51, arc52);
	}

	public void setCenterXY(Point2D point) {
		setCenterXY(point.getX(), point.getY());
	}

	public void setCenterXY(double x, double y) {
		front.setCenterX(x);
		front.setCenterY(y);
		arc1.setCenterX(x);
		arc1.setCenterY(y);
		arc2.setCenterX(x);
		arc2.setCenterY(y);
		arc3.setCenterX(x);
		arc3.setCenterY(y);
		arc41.setCenterX(x);
		arc41.setCenterY(y);
		arc42.setCenterX(x);
		arc42.setCenterY(y);
		arc51.setCenterX(x);
		arc51.setCenterY(y);
		arc52.setCenterX(x);
		arc52.setCenterY(y);
//		System.out.println("x: " + x + " y: " + y);

		setRotateTransition();
	}

	public void setRadius(double r) {
		r -= 27;
		front.setRadius(r);
		arc1.setRadiusX(r + 3);
		arc1.setRadiusY(r + 3);
		arc2.setRadiusX(r + 9);
		arc2.setRadiusY(r + 9);
		arc3.setRadiusX(r + 15);
		arc3.setRadiusY(r + 15);
		arc41.setRadiusX(r + 21);
		arc41.setRadiusY(r + 21);
		arc42.setRadiusX(r + 21);
		arc42.setRadiusY(r + 21);
		arc51.setRadiusX(r + 27);
		arc51.setRadiusY(r + 27);
		arc52.setRadiusX(r + 27);
		arc52.setRadiusY(r + 27);
	}

	private void setRotateTransition() {
		RotateTransition rt1 = new RotateTransition(Duration.seconds(5), arc1);
		rt1.setByAngle(360);
		rt1.setInterpolator(Interpolator.LINEAR);
		rt1.setCycleCount(Animation.INDEFINITE);
		rt1.play();

		RotateTransition rt2 = new RotateTransition(Duration.seconds(5), arc2);
		rt2.setByAngle(-360);
		rt2.setInterpolator(Interpolator.LINEAR);
		rt2.setCycleCount(Animation.INDEFINITE);
		rt2.play();

		RotateTransition rt3 = new RotateTransition(Duration.seconds(5), arc3);
		rt3.setByAngle(360);
		rt3.setInterpolator(Interpolator.LINEAR);
		rt3.setCycleCount(Animation.INDEFINITE);
		rt3.play();

		RotateTransition rt4 = new RotateTransition(Duration.seconds(5), arc4gr);
		rt4.setByAngle(360);
		rt4.setInterpolator(Interpolator.LINEAR);
		rt4.setCycleCount(Animation.INDEFINITE);
		rt4.play();

		RotateTransition rt5 = new RotateTransition(Duration.seconds(5), arc5gr);
		rt5.setByAngle(-360);
		rt5.setInterpolator(Interpolator.LINEAR);
		rt5.setCycleCount(Animation.INDEFINITE);
		rt5.play();
	}

	public static ItemCircle createLoketCircle(Image image) {
		Circle circle = new Circle();
		circle.setFill(new ImagePattern(image));

		ItemCircle result = new ItemCircle(circle);
		return result;
	}
}
