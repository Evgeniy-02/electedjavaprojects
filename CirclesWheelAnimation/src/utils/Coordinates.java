package utils;

import javafx.geometry.Point2D;
import static java.lang.Math.*;

public class Coordinates {

	public static PolarPoint2D toPolarSystem(double x, double y) {
		double radius = sqrt(x * x + y * y);
		double angle = atan2(y, x);
		return new PolarPoint2D(radius, angle/* > 0 ? angle : (angle + 2 * PI) */);
	}

	public static PolarPoint2D toPolarSystem(Point2D point) {
		return toPolarSystem(point.getX(), point.getY());
	}

	public static Point2D toCartesianSystem(double radius, double angle) {
		double x = radius * Math.cos(angle);
		double y = radius * Math.sin(angle);

		return new Point2D(x, y);
	}

	public static Point2D toCartesianSystem(PolarPoint2D point) {
		return toCartesianSystem(point.radius, point.angle);
	}
}
