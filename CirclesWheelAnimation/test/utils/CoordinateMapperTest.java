package utils;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Test;

import javafx.geometry.Point2D;

class CoordinateMapperTest {
	CoordinateMapper mapper = new CoordinateMapper(new Point2D(300, 200));

	Point2D[] scenePoints = { new Point2D(0, 0), new Point2D(300, 50), new Point2D(300, 200), new Point2D(400, 200),
			new Point2D(200, 350), new Point2D(300, 350) };
	Point2D[] localPoints = { new Point2D(-300, 200), new Point2D(0, 150), new Point2D(0, 0), new Point2D(100, 0),
			new Point2D(-100, -150), new Point2D(0, -150) };

	@Test
	void testMapToSceneCoordinates() {
		Point2D[] result = new Point2D[localPoints.length];
		for (int i = 0; i < localPoints.length; i++) {
			result[i] = mapper.mapToPaneCoordinates(localPoints[i]);
		}

		assertArrayEquals(scenePoints, result);
	}

	@Test
	void testMapToLocalCoordinates() {
		Point2D[] result = new Point2D[scenePoints.length];
		for (int i = 0; i < scenePoints.length; i++) {
			result[i] = mapper.mapToLocalCoordinates(scenePoints[i]);
		}

		assertArrayEquals(localPoints, result);
	}

}
