package utils;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
//import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.TestMethodOrder;

import javafx.geometry.Point2D;

//@TestMethodOrder(OrderAnnotation.class)
class CoordinatesTest {
	static Map<Point2D, PolarPoint2D> points = new HashMap<Point2D, PolarPoint2D>(20);
	public static final double EPSILON = 1e-4;

	@BeforeAll
	static void setUpBeforeClass() {
		points.put(new Point2D(1, 1), new PolarPoint2D(1.41421, 0.78540));
		points.put(new Point2D(4, 1), new PolarPoint2D(4.12311, 0.24498));
		points.put(new Point2D(12, 7), new PolarPoint2D(13.89244, 0.52807));
		points.put(new Point2D(2, 7), new PolarPoint2D(7.28011, 1.29250));
		points.put(new Point2D(-15, 5), new PolarPoint2D(15.81139, 2.81984));
		points.put(new Point2D(-15, -9), new PolarPoint2D(17.49286, 2 * Math.PI - 2.60117));
		points.put(new Point2D(4, -9), new PolarPoint2D(9.84886, 2 * Math.PI - 1.15257));

	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	//@Order(1)
//	@Disabled
	@Test
	@DisplayName("😱")
	void test() {
		for (Map.Entry<Point2D, PolarPoint2D> entry : points.entrySet()) {
			System.out.println(Coordinates.toCartesianSystem(entry.getValue()) + " ? " + entry.getKey());
			System.out.println(Coordinates.toPolarSystem(entry.getKey()) + " ? " + entry.getValue());
			System.out.println("*************************************");
		}
	}

	//@Order(2)
	@Test
	void testToCartesianCoordinates() {
		for (Map.Entry<Point2D, PolarPoint2D> entry : points.entrySet()) {
			System.out.print(entry.getKey());
			assertEquals("x: ", entry.getKey().getX(), Coordinates.toCartesianSystem(entry.getValue()).getX(),
					EPSILON);
			assertEquals("y: ", entry.getKey().getY(), Coordinates.toCartesianSystem(entry.getValue()).getY(),
					EPSILON);
			System.out.println(" [OK]");
			System.out.println("**********************************************");
		}
	}

	//@Order(3)
	@Test
	void testToPolarCoordinates() {
		for (Map.Entry<Point2D, PolarPoint2D> entry : points.entrySet()) {
			System.out.print(entry.getValue());
			assertEquals("r: ", entry.getValue().radius, Coordinates.toPolarSystem(entry.getKey()).radius,
					EPSILON);
			assertEquals("phi: ", entry.getValue().angle, Coordinates.toPolarSystem(entry.getKey()).angle,
					EPSILON);
			System.out.println(" [OK]");
			System.out.println("**********************************************");
		}
	}

	@Test
	void test1() {
		assert 1 == 2 : "1 == 2 FALSE";
	}

}
