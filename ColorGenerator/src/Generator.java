import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.UIManager;
import javax.swing.JPanel;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;
import javax.swing.SwingConstants;
import javax.swing.JTextField;

/**
 * Color generator
 * 
 * @author Evgeniy
 */

public class Generator {

	private JFrame frame;
	private JSlider rslider;
	private JSlider gslider;
	private JSlider bslider;
	private JPanel colorPanel;
	Color x = Color.WHITE;
	int r;
	int g;
	int b;
	Color titlColor;
	private JTextField textField;
	private Random m = new Random();
	ArrayList<Color> colorList;
	private JButton pButton;
	private JTextField colorTextField;

	/**
	 * 
	 * Отображение сгенерированных случайных и заданных цветов
	 * 
	 */

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Generator window = new Generator();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Generator() {
		initialize();
	}

	private void initialize() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}

		colorList = new ArrayList<Color>();

		frame = new JFrame();
		frame.setBounds(m.nextInt(850), m.nextInt(300), 550, 400);
		frame.setPreferredSize(new Dimension(450, 350));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.getContentPane().setBackground(new Color(175, 238, 238));
		frame.setTitle("COLOR GENERATOR");
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 30, 15, 45, 45, 30, 145, 30, 30, 45, 45, 45, 30 };
		gridBagLayout.rowHeights = new int[] { 20, 30, 10, 30, 30, 30, 30, 30, 30, 10, 30, 20 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0 };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0 };
		frame.getContentPane().setLayout(gridBagLayout);

		textField = new JTextField();
		textField.setFont(new Font("Dialog", Font.BOLD, 13));
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.fill = GridBagConstraints.BOTH;
		gbc_textField.gridwidth = 9;
		gbc_textField.gridx = 2;
		gbc_textField.gridy = 1;
		String colorStr = " This Color (" + x.getRed() + "," + x.getGreen() + "," + x.getBlue() + ")";
		textField.setText(colorStr);
		frame.getContentPane().add(textField, gbc_textField);
		textField.setColumns(10);

		JLabel rLabel = new JLabel("Red");
		rLabel.setFont(new Font("Dialog", Font.BOLD, 14));
		rLabel.setForeground(Color.RED);
		GridBagConstraints gbc_rLabel = new GridBagConstraints();
		gbc_rLabel.insets = new Insets(0, 0, 5, 5);
		gbc_rLabel.gridx = 2;
		gbc_rLabel.gridy = 2;
		frame.getContentPane().add(rLabel, gbc_rLabel);

		rslider = new JSlider();
		rslider.setMaximum(255);
		rslider.setValue(x.getRed());
		rslider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				applySlider();
				r = rslider.getValue();
			}
		});
		GridBagConstraints gbc_rslider = new GridBagConstraints();
		gbc_rslider.gridwidth = 8;
		gbc_rslider.fill = GridBagConstraints.HORIZONTAL;
		gbc_rslider.insets = new Insets(0, 0, 5, 5);
		gbc_rslider.gridx = 3;
		gbc_rslider.gridy = 2;
		frame.getContentPane().add(rslider, gbc_rslider);

		JLabel gLabel = new JLabel("Green");
		gLabel.setFont(new Font("Dialog", Font.BOLD, 14));
		gLabel.setForeground(Color.GREEN);
		GridBagConstraints gbc_gLabel = new GridBagConstraints();
		gbc_gLabel.insets = new Insets(0, 0, 5, 5);
		gbc_gLabel.gridx = 2;
		gbc_gLabel.gridy = 3;
		frame.getContentPane().add(gLabel, gbc_gLabel);

		gslider = new JSlider();
		gslider.setMaximum(255);
		gslider.setValue(x.getGreen());
		gslider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				applySlider();
				g = gslider.getValue();
			}
		});
		GridBagConstraints gbc_gslider = new GridBagConstraints();
		gbc_gslider.fill = GridBagConstraints.HORIZONTAL;
		gbc_gslider.gridwidth = 8;
		gbc_gslider.insets = new Insets(0, 0, 5, 5);
		gbc_gslider.gridx = 3;
		gbc_gslider.gridy = 3;
		frame.getContentPane().add(gslider, gbc_gslider);

		JLabel bLabel = new JLabel("Blue");
		bLabel.setFont(new Font("Dialog", Font.BOLD, 14));
		bLabel.setForeground(Color.BLUE);
		GridBagConstraints gbc_bLabel = new GridBagConstraints();
		gbc_bLabel.insets = new Insets(0, 0, 5, 5);
		gbc_bLabel.gridx = 2;
		gbc_bLabel.gridy = 4;
		frame.getContentPane().add(bLabel, gbc_bLabel);

		bslider = new JSlider();
		bslider.setMaximum(255);
		bslider.setValue(x.getBlue());
		bslider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				applySlider();
				b = bslider.getValue();
			}
		});
		GridBagConstraints gbc_bslider = new GridBagConstraints();
		gbc_bslider.fill = GridBagConstraints.HORIZONTAL;
		gbc_bslider.gridwidth = 8;
		gbc_bslider.insets = new Insets(0, 0, 5, 5);
		gbc_bslider.gridx = 3;
		gbc_bslider.gridy = 4;
		frame.getContentPane().add(bslider, gbc_bslider);

		colorPanel = new JPanel();
		colorPanel.setBackground(Color.WHITE);
		colorPanel.setBorder(new TitledBorder(new LineBorder(titlColor, 2), "Color generator", TitledBorder.LEADING,
				TitledBorder.TOP, null, titlColor));
		GridBagConstraints gbc_colorPanel = new GridBagConstraints();
		gbc_colorPanel.gridheight = 5;
		gbc_colorPanel.gridwidth = 9;
		gbc_colorPanel.insets = new Insets(0, 0, 5, 5);
		gbc_colorPanel.fill = GridBagConstraints.BOTH;
		gbc_colorPanel.gridx = 2;
		gbc_colorPanel.gridy = 5;
		frame.getContentPane().add(colorPanel, gbc_colorPanel);

		JButton nButton = new JButton("Next color");
		nButton.setForeground(new Color(119, 136, 153));
		nButton.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				colorList.add(colorPanel.getBackground());
				applyNextButton();
			}
		});

		colorTextField = new JTextField();
		colorTextField.setHorizontalAlignment(SwingConstants.CENTER);
		colorTextField.setFont(new Font("Dialog", Font.BOLD, 13));
		GridBagConstraints gbc_colorTextField = new GridBagConstraints();
		gbc_colorTextField.weightx = 1.0;
		colorTextField.setText("Colour text");
		gbc_colorTextField.gridwidth = 3;
		gbc_colorTextField.insets = new Insets(10, 0, 5, 5);
		gbc_colorTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_colorTextField.gridx = 5;
		gbc_colorTextField.gridy = 10;
		frame.getContentPane().add(colorTextField, gbc_colorTextField);
		colorTextField.setColumns(10);

		GridBagConstraints gbc_nButton = new GridBagConstraints();
		gbc_nButton.insets = new Insets(10, 0, 5, 5);
		gbc_nButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_nButton.gridwidth = 3;
		gbc_nButton.gridx = 8;
		gbc_nButton.gridy = 10;
		frame.getContentPane().add(nButton, gbc_nButton);

		pButton = new JButton("Back color");
		pButton.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				int n = colorList.size();
				if (n > 0) {
					Color v = colorList.get(n - 1);
					applyColorToWindow(v);
					colorList.remove(v);
				}
			}
		});
		pButton.setForeground(new Color(104, 162, 109));
		GridBagConstraints gbc_pButton = new GridBagConstraints();
		gbc_pButton.gridwidth = 3;
		gbc_pButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_pButton.insets = new Insets(10, 0, 5, 5);
		gbc_pButton.gridx = 2;
		gbc_pButton.gridy = 10;
		frame.getContentPane().add(pButton, gbc_pButton);
	}

	void applySlider() {
		r = rslider.getValue();
		g = gslider.getValue();
		b = bslider.getValue();
		Color color = new Color(r, g, b);
		colorPanel.setBackground(color);
		colorTextField.setForeground(color);

		String colorStr = " This Color (" + r + "," + g + "," + b + ")";
		textField.setText(colorStr);

		// Меняет цвет рамки "Color generator"
		titlColor = (r + g + b > 255 * 3 / 2) ? Color.BLACK : Color.WHITE;

		colorPanel.setBorder(new TitledBorder(new LineBorder(titlColor, 2), "Color generator", TitledBorder.LEADING,
				TitledBorder.TOP, null, titlColor));
	}

	void applyNextButton() {
		x = new Color(m.nextInt(256), m.nextInt(256), m.nextInt(256));
		applyColorToWindow(x);
	}

	void applyColorToWindow(Color c) {
		colorPanel.setBackground(c);
		colorTextField.setForeground(c);
		String colorStr = " This Color (" + c.getRed() + "," + c.getGreen() + "," + c.getBlue() + ")";
		textField.setText(colorStr);

		rslider.setValue(c.getRed());
		gslider.setValue(c.getGreen());
		bslider.setValue(c.getBlue());
	}
}